cmd_arch/arm/boot/dts/zynq-zybo.dtb := mkdir -p arch/arm/boot/dts/ ; arm-linux-gnueabihf-gcc -E -Wp,-MD,arch/arm/boot/dts/.zynq-zybo.dtb.d.pre.tmp -nostdinc -I./arch/arm/boot/dts -I./arch/arm/boot/dts/include -I./drivers/of/testcase-data -undef -D__DTS__ -x assembler-with-cpp -o arch/arm/boot/dts/.zynq-zybo.dtb.dts.tmp arch/arm/boot/dts/zynq-zybo.dts ; ./scripts/dtc/dtc -O dtb -o arch/arm/boot/dts/zynq-zybo.dtb -b 0 -i arch/arm/boot/dts/  -d arch/arm/boot/dts/.zynq-zybo.dtb.d.dtc.tmp arch/arm/boot/dts/.zynq-zybo.dtb.dts.tmp ; cat arch/arm/boot/dts/.zynq-zybo.dtb.d.pre.tmp arch/arm/boot/dts/.zynq-zybo.dtb.d.dtc.tmp > arch/arm/boot/dts/.zynq-zybo.dtb.d

source_arch/arm/boot/dts/zynq-zybo.dtb := arch/arm/boot/dts/zynq-zybo.dts

deps_arch/arm/boot/dts/zynq-zybo.dtb := \
  arch/arm/boot/dts/zynq-7000.dtsi \
  arch/arm/boot/dts/skeleton.dtsi \

arch/arm/boot/dts/zynq-zybo.dtb: $(deps_arch/arm/boot/dts/zynq-zybo.dtb)

$(deps_arch/arm/boot/dts/zynq-zybo.dtb):
