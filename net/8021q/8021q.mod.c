#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x65137d02, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x557240d8, __VMLINUX_SYMBOL_STR(register_netdevice) },
	{ 0x9d55148e, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0xfbc74f64, __VMLINUX_SYMBOL_STR(__copy_from_user) },
	{ 0x469f3773, __VMLINUX_SYMBOL_STR(dev_change_flags) },
	{ 0x27500a07, __VMLINUX_SYMBOL_STR(dev_mc_unsync) },
	{ 0x70f42fd4, __VMLINUX_SYMBOL_STR(single_open) },
	{ 0x67c2fa54, __VMLINUX_SYMBOL_STR(__copy_to_user) },
	{ 0x60a13e90, __VMLINUX_SYMBOL_STR(rcu_barrier) },
	{ 0xed0970f5, __VMLINUX_SYMBOL_STR(dev_get_nest_level) },
	{ 0x3c6b9125, __VMLINUX_SYMBOL_STR(vlan_dev_vlan_id) },
	{ 0xea85737, __VMLINUX_SYMBOL_STR(dev_uc_add) },
	{ 0x51360db1, __VMLINUX_SYMBOL_STR(single_release) },
	{ 0x6f86333c, __VMLINUX_SYMBOL_STR(seq_puts) },
	{ 0xc7a4fbed, __VMLINUX_SYMBOL_STR(rtnl_lock) },
	{ 0x65fbb669, __VMLINUX_SYMBOL_STR(vlan_uses_dev) },
	{ 0x4cdd6905, __VMLINUX_SYMBOL_STR(netif_carrier_on) },
	{ 0xd3f57a2, __VMLINUX_SYMBOL_STR(_find_next_bit_le) },
	{ 0x5c43cb42, __VMLINUX_SYMBOL_STR(seq_printf) },
	{ 0xd2da1048, __VMLINUX_SYMBOL_STR(register_netdevice_notifier) },
	{ 0x81c45386, __VMLINUX_SYMBOL_STR(netif_carrier_off) },
	{ 0xe412eb75, __VMLINUX_SYMBOL_STR(remove_proc_entry) },
	{ 0x50c89f23, __VMLINUX_SYMBOL_STR(__alloc_percpu) },
	{ 0x62e9262b, __VMLINUX_SYMBOL_STR(dev_set_allmulti) },
	{ 0xf16b60cc, __VMLINUX_SYMBOL_STR(vlan_vid_del) },
	{ 0xeb398782, __VMLINUX_SYMBOL_STR(call_netdevice_notifiers) },
	{ 0x511c82d9, __VMLINUX_SYMBOL_STR(linkwatch_fire_event) },
	{ 0xdd0ae802, __VMLINUX_SYMBOL_STR(vlan_vid_add) },
	{ 0xbcc77c87, __VMLINUX_SYMBOL_STR(seq_read) },
	{ 0xc9ec4e21, __VMLINUX_SYMBOL_STR(free_percpu) },
	{ 0x9d0d6206, __VMLINUX_SYMBOL_STR(unregister_netdevice_notifier) },
	{ 0xe2d5255a, __VMLINUX_SYMBOL_STR(strcmp) },
	{ 0x831f1e88, __VMLINUX_SYMBOL_STR(proc_remove) },
	{ 0x8411c272, __VMLINUX_SYMBOL_STR(vlan_ioctl_set) },
	{ 0x425bf346, __VMLINUX_SYMBOL_STR(PDE_DATA) },
	{ 0xfe7c4287, __VMLINUX_SYMBOL_STR(nr_cpu_ids) },
	{ 0xfa2a45e, __VMLINUX_SYMBOL_STR(__memzero) },
	{ 0x2ac9c4b8, __VMLINUX_SYMBOL_STR(unregister_pernet_subsys) },
	{ 0x9fdecc31, __VMLINUX_SYMBOL_STR(unregister_netdevice_many) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0xb6c80913, __VMLINUX_SYMBOL_STR(ethtool_op_get_link) },
	{ 0x5c54b7e6, __VMLINUX_SYMBOL_STR(ns_capable) },
	{ 0xfcc14d6f, __VMLINUX_SYMBOL_STR(__ethtool_get_settings) },
	{ 0x826be9b0, __VMLINUX_SYMBOL_STR(free_netdev) },
	{ 0x328a05f1, __VMLINUX_SYMBOL_STR(strncpy) },
	{ 0x17fcc2e7, __VMLINUX_SYMBOL_STR(nla_put) },
	{ 0xcc0ff702, __VMLINUX_SYMBOL_STR(netdev_upper_dev_unlink) },
	{ 0x73e20c1c, __VMLINUX_SYMBOL_STR(strlcpy) },
	{ 0xc956e94e, __VMLINUX_SYMBOL_STR(skb_push) },
	{ 0x891e62cb, __VMLINUX_SYMBOL_STR(proc_mkdir_data) },
	{ 0x3f56e11f, __VMLINUX_SYMBOL_STR(seq_release_net) },
	{ 0x9794cb9b, __VMLINUX_SYMBOL_STR(netif_stacked_transfer_operstate) },
	{ 0x2469810f, __VMLINUX_SYMBOL_STR(__rcu_read_unlock) },
	{ 0x67f06647, __VMLINUX_SYMBOL_STR(init_net) },
	{ 0xb1757306, __VMLINUX_SYMBOL_STR(rtnl_link_unregister) },
	{ 0xc223ee08, __VMLINUX_SYMBOL_STR(__dev_get_by_index) },
	{ 0x347013de, __VMLINUX_SYMBOL_STR(nla_validate) },
	{ 0xecb465c0, __VMLINUX_SYMBOL_STR(kmem_cache_alloc) },
	{ 0x4c043dd1, __VMLINUX_SYMBOL_STR(arp_find) },
	{ 0xd3e6f60d, __VMLINUX_SYMBOL_STR(cpu_possible_mask) },
	{ 0x2e2980a1, __VMLINUX_SYMBOL_STR(eth_header_parse) },
	{ 0x6b2dc060, __VMLINUX_SYMBOL_STR(dump_stack) },
	{ 0x312c7884, __VMLINUX_SYMBOL_STR(alloc_netdev_mqs) },
	{ 0x6606ee7d, __VMLINUX_SYMBOL_STR(register_pernet_subsys) },
	{ 0xeb44dbde, __VMLINUX_SYMBOL_STR(netdev_upper_dev_link) },
	{ 0xa5972631, __VMLINUX_SYMBOL_STR(ether_setup) },
	{ 0x122c5c5c, __VMLINUX_SYMBOL_STR(dev_uc_unsync) },
	{ 0xff04f52e, __VMLINUX_SYMBOL_STR(__dev_get_by_name) },
	{ 0x341dbfa3, __VMLINUX_SYMBOL_STR(__per_cpu_offset) },
	{ 0x46f582e9, __VMLINUX_SYMBOL_STR(unregister_netdevice_queue) },
	{ 0xe23cf187, __VMLINUX_SYMBOL_STR(netdev_warn) },
	{ 0x1b999337, __VMLINUX_SYMBOL_STR(proc_create_data) },
	{ 0x24528cc0, __VMLINUX_SYMBOL_STR(eth_validate_addr) },
	{ 0x4a04fa2d, __VMLINUX_SYMBOL_STR(seq_lseek) },
	{ 0xfde9ffed, __VMLINUX_SYMBOL_STR(dev_set_promiscuity) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x9d669763, __VMLINUX_SYMBOL_STR(memcpy) },
	{ 0xf188de36, __VMLINUX_SYMBOL_STR(seq_open_net) },
	{ 0xf6b85413, __VMLINUX_SYMBOL_STR(rtnl_link_register) },
	{ 0x5a97942f, __VMLINUX_SYMBOL_STR(dev_uc_del) },
	{ 0x6e2ab255, __VMLINUX_SYMBOL_STR(dev_uc_sync) },
	{ 0xefd6cf06, __VMLINUX_SYMBOL_STR(__aeabi_unwind_cpp_pr0) },
	{ 0xb81960ca, __VMLINUX_SYMBOL_STR(snprintf) },
	{ 0x6f70eabc, __VMLINUX_SYMBOL_STR(netdev_update_features) },
	{ 0x85670f1d, __VMLINUX_SYMBOL_STR(rtnl_is_locked) },
	{ 0x5b298432, __VMLINUX_SYMBOL_STR(dev_queue_xmit) },
	{ 0x8d522714, __VMLINUX_SYMBOL_STR(__rcu_read_lock) },
	{ 0x9cab1c26, __VMLINUX_SYMBOL_STR(dev_mc_sync) },
	{ 0x6e720ff2, __VMLINUX_SYMBOL_STR(rtnl_unlock) },
	{ 0x5fae3b17, __VMLINUX_SYMBOL_STR(dev_get_stats) },
	{ 0x3cb584ab, __VMLINUX_SYMBOL_STR(dev_set_mtu) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";


MODULE_INFO(srcversion, "7E9F42FA57AE89FD39C1892");
