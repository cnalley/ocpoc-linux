#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x65137d02, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0xc55ade1b, __VMLINUX_SYMBOL_STR(ip_tunnel_get_link_net) },
	{ 0x60ee9172, __VMLINUX_SYMBOL_STR(param_ops_bool) },
	{ 0xb29e2e34, __VMLINUX_SYMBOL_STR(ip_tunnel_get_stats64) },
	{ 0xf6b85413, __VMLINUX_SYMBOL_STR(rtnl_link_register) },
	{ 0x723f1a47, __VMLINUX_SYMBOL_STR(xfrm4_tunnel_register) },
	{ 0xc492553f, __VMLINUX_SYMBOL_STR(register_pernet_device) },
	{ 0x60a13e90, __VMLINUX_SYMBOL_STR(rcu_barrier) },
	{ 0x885d7ffa, __VMLINUX_SYMBOL_STR(unregister_pernet_device) },
	{ 0x98f0ef60, __VMLINUX_SYMBOL_STR(xfrm4_tunnel_deregister) },
	{ 0xb1757306, __VMLINUX_SYMBOL_STR(rtnl_link_unregister) },
	{ 0xecd2add6, __VMLINUX_SYMBOL_STR(netif_rx) },
	{ 0x26289d9d, __VMLINUX_SYMBOL_STR(skb_scrub_packet) },
	{ 0xf2327d63, __VMLINUX_SYMBOL_STR(ipv6_chk_prefix) },
	{ 0x9dde3a55, __VMLINUX_SYMBOL_STR(ipv6_chk_custom_prefix) },
	{ 0x9d55148e, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0xc6cbbc89, __VMLINUX_SYMBOL_STR(capable) },
	{ 0xecb465c0, __VMLINUX_SYMBOL_STR(kmem_cache_alloc) },
	{ 0x9469482, __VMLINUX_SYMBOL_STR(kfree_call_rcu) },
	{ 0xbc10dd97, __VMLINUX_SYMBOL_STR(__put_user_4) },
	{ 0x12da5bb2, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0x67c2fa54, __VMLINUX_SYMBOL_STR(__copy_to_user) },
	{ 0x5c54b7e6, __VMLINUX_SYMBOL_STR(ns_capable) },
	{ 0xfbc74f64, __VMLINUX_SYMBOL_STR(__copy_from_user) },
	{ 0xa0e047d2, __VMLINUX_SYMBOL_STR(netdev_state_change) },
	{ 0x609f1c7e, __VMLINUX_SYMBOL_STR(synchronize_net) },
	{ 0x20f858f6, __VMLINUX_SYMBOL_STR(ip_tunnel_dst_reset_all) },
	{ 0x32ddd5b6, __VMLINUX_SYMBOL_STR(call_rcu) },
	{ 0x6b2dc060, __VMLINUX_SYMBOL_STR(dump_stack) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x85670f1d, __VMLINUX_SYMBOL_STR(rtnl_is_locked) },
	{ 0xcd4630cd, __VMLINUX_SYMBOL_STR(sock_wfree) },
	{ 0xf6ebc03b, __VMLINUX_SYMBOL_STR(net_ratelimit) },
	{ 0x8e198955, __VMLINUX_SYMBOL_STR(iptunnel_xmit) },
	{ 0x2ad52d27, __VMLINUX_SYMBOL_STR(ip_tunnel_encap) },
	{ 0xe983573d, __VMLINUX_SYMBOL_STR(consume_skb) },
	{ 0x1722f316, __VMLINUX_SYMBOL_STR(skb_realloc_headroom) },
	{ 0x6c9d8955, __VMLINUX_SYMBOL_STR(neigh_destroy) },
	{ 0xd542439, __VMLINUX_SYMBOL_STR(__ipv6_addr_type) },
	{ 0x9cc2b638, __VMLINUX_SYMBOL_STR(ip_tunnel_xmit) },
	{ 0x6ab81fc2, __VMLINUX_SYMBOL_STR(iptunnel_handle_offloads) },
	{ 0x7d11c268, __VMLINUX_SYMBOL_STR(jiffies) },
	{ 0x6d193995, __VMLINUX_SYMBOL_STR(icmpv6_send) },
	{ 0xe23b2ca9, __VMLINUX_SYMBOL_STR(rt6_lookup) },
	{ 0xb8f2a833, __VMLINUX_SYMBOL_STR(skb_pull) },
	{ 0x74571971, __VMLINUX_SYMBOL_STR(__pskb_pull_tail) },
	{ 0x789ef6c8, __VMLINUX_SYMBOL_STR(skb_clone) },
	{ 0x7dd119cf, __VMLINUX_SYMBOL_STR(ipv4_redirect) },
	{ 0x77c5e82a, __VMLINUX_SYMBOL_STR(ipv4_update_pmtu) },
	{ 0x908457a5, __VMLINUX_SYMBOL_STR(ip_tunnel_rcv) },
	{ 0x1a50772a, __VMLINUX_SYMBOL_STR(iptunnel_pull_header) },
	{ 0x9917c043, __VMLINUX_SYMBOL_STR(kfree_skb) },
	{ 0x198d145a, __VMLINUX_SYMBOL_STR(__xfrm_policy_check) },
	{ 0xa7a866aa, __VMLINUX_SYMBOL_STR(register_netdev) },
	{ 0x6e720ff2, __VMLINUX_SYMBOL_STR(rtnl_unlock) },
	{ 0x9fdecc31, __VMLINUX_SYMBOL_STR(unregister_netdevice_many) },
	{ 0xc7a4fbed, __VMLINUX_SYMBOL_STR(rtnl_lock) },
	{ 0x341dbfa3, __VMLINUX_SYMBOL_STR(__per_cpu_offset) },
	{ 0xfe7c4287, __VMLINUX_SYMBOL_STR(nr_cpu_ids) },
	{ 0xd3e6f60d, __VMLINUX_SYMBOL_STR(cpu_possible_mask) },
	{ 0xd3f57a2, __VMLINUX_SYMBOL_STR(_find_next_bit_le) },
	{ 0x50c89f23, __VMLINUX_SYMBOL_STR(__alloc_percpu) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0xc223ee08, __VMLINUX_SYMBOL_STR(__dev_get_by_index) },
	{ 0x98374df9, __VMLINUX_SYMBOL_STR(dst_release) },
	{ 0x1ed94995, __VMLINUX_SYMBOL_STR(ip_route_output_flow) },
	{ 0xa6c8a46c, __VMLINUX_SYMBOL_STR(ip_tunnel_encap_setup) },
	{ 0x312c7884, __VMLINUX_SYMBOL_STR(alloc_netdev_mqs) },
	{ 0xe914e41e, __VMLINUX_SYMBOL_STR(strcpy) },
	{ 0x73e20c1c, __VMLINUX_SYMBOL_STR(strlcpy) },
	{ 0x826be9b0, __VMLINUX_SYMBOL_STR(free_netdev) },
	{ 0xc9ec4e21, __VMLINUX_SYMBOL_STR(free_percpu) },
	{ 0x557240d8, __VMLINUX_SYMBOL_STR(register_netdevice) },
	{ 0xfa2a45e, __VMLINUX_SYMBOL_STR(__memzero) },
	{ 0x67f06647, __VMLINUX_SYMBOL_STR(init_net) },
	{ 0x46f582e9, __VMLINUX_SYMBOL_STR(unregister_netdevice_queue) },
	{ 0x2469810f, __VMLINUX_SYMBOL_STR(__rcu_read_unlock) },
	{ 0x8d522714, __VMLINUX_SYMBOL_STR(__rcu_read_lock) },
	{ 0x17fcc2e7, __VMLINUX_SYMBOL_STR(nla_put) },
	{ 0xefd6cf06, __VMLINUX_SYMBOL_STR(__aeabi_unwind_cpp_pr0) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=ip_tunnel,tunnel4,ipv6";

