#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x65137d02, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0xc55ade1b, __VMLINUX_SYMBOL_STR(ip_tunnel_get_link_net) },
	{ 0x4e21b813, __VMLINUX_SYMBOL_STR(ip_tunnel_dellink) },
	{ 0x60ee9172, __VMLINUX_SYMBOL_STR(param_ops_bool) },
	{ 0xb29e2e34, __VMLINUX_SYMBOL_STR(ip_tunnel_get_stats64) },
	{ 0xf5b6a58a, __VMLINUX_SYMBOL_STR(ip_tunnel_change_mtu) },
	{ 0x5d0855dd, __VMLINUX_SYMBOL_STR(ip_tunnel_uninit) },
	{ 0xb1757306, __VMLINUX_SYMBOL_STR(rtnl_link_unregister) },
	{ 0x885d7ffa, __VMLINUX_SYMBOL_STR(unregister_pernet_device) },
	{ 0x98f0ef60, __VMLINUX_SYMBOL_STR(xfrm4_tunnel_deregister) },
	{ 0xf6b85413, __VMLINUX_SYMBOL_STR(rtnl_link_register) },
	{ 0x723f1a47, __VMLINUX_SYMBOL_STR(xfrm4_tunnel_register) },
	{ 0xc492553f, __VMLINUX_SYMBOL_STR(register_pernet_device) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x777f4ee2, __VMLINUX_SYMBOL_STR(ip_tunnel_init_net) },
	{ 0xef0aeff, __VMLINUX_SYMBOL_STR(ip_tunnel_delete_net) },
	{ 0x908457a5, __VMLINUX_SYMBOL_STR(ip_tunnel_rcv) },
	{ 0x1a50772a, __VMLINUX_SYMBOL_STR(iptunnel_pull_header) },
	{ 0x198d145a, __VMLINUX_SYMBOL_STR(__xfrm_policy_check) },
	{ 0x7d11c268, __VMLINUX_SYMBOL_STR(jiffies) },
	{ 0x67f06647, __VMLINUX_SYMBOL_STR(init_net) },
	{ 0x7dd119cf, __VMLINUX_SYMBOL_STR(ipv4_redirect) },
	{ 0x77c5e82a, __VMLINUX_SYMBOL_STR(ipv4_update_pmtu) },
	{ 0xd104be03, __VMLINUX_SYMBOL_STR(ip_tunnel_lookup) },
	{ 0x2469810f, __VMLINUX_SYMBOL_STR(__rcu_read_unlock) },
	{ 0x8d522714, __VMLINUX_SYMBOL_STR(__rcu_read_lock) },
	{ 0x50f61db1, __VMLINUX_SYMBOL_STR(ip_tunnel_init) },
	{ 0x9917c043, __VMLINUX_SYMBOL_STR(kfree_skb) },
	{ 0x9cc2b638, __VMLINUX_SYMBOL_STR(ip_tunnel_xmit) },
	{ 0x6ab81fc2, __VMLINUX_SYMBOL_STR(iptunnel_handle_offloads) },
	{ 0x67c2fa54, __VMLINUX_SYMBOL_STR(__copy_to_user) },
	{ 0x8fda14e8, __VMLINUX_SYMBOL_STR(ip_tunnel_ioctl) },
	{ 0xfbc74f64, __VMLINUX_SYMBOL_STR(__copy_from_user) },
	{ 0xd41468e6, __VMLINUX_SYMBOL_STR(ip_tunnel_setup) },
	{ 0x7b2cea13, __VMLINUX_SYMBOL_STR(ip_tunnel_newlink) },
	{ 0xda794252, __VMLINUX_SYMBOL_STR(ip_tunnel_changelink) },
	{ 0xa6c8a46c, __VMLINUX_SYMBOL_STR(ip_tunnel_encap_setup) },
	{ 0xfa2a45e, __VMLINUX_SYMBOL_STR(__memzero) },
	{ 0x17fcc2e7, __VMLINUX_SYMBOL_STR(nla_put) },
	{ 0xefd6cf06, __VMLINUX_SYMBOL_STR(__aeabi_unwind_cpp_pr0) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=ip_tunnel,tunnel4";

