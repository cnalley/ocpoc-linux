# ocpoc-linux

This kernel build follows the instructions here:
https://aerotenna.readme.io/v1.0/docs/develop-your-kernel
i.e.
git clone https://github.com/Xilinx/linux-xlnx.git
git clone https://github.com/aerotenna/ocpoc_mini_zynq_files.git
cp ocpoc_mini_zynq_files/Kernel_Config/ocpoc_defconfig linux-xlnx/arch/arm/configs
cd linux-xlnx
git checkout xilinx-v2015.4
git checkout xilinx-v2016.2 -- drivers/i2c/busses/i2c-xiic.c
make ARCH=arm ocpoc_defconfig
make ARCH=arm menuconfig
make ARCH=arm UIMAGE_LOADADDR=0x8000 uImage

But the .config is copied from https://bitbucket.org/windhoverlabs/linux-xlnx
and the linux uvc module turned on in menuconfig device drivers, multimedia support, media USB adapters, any options mentioning uvc.
and all rtl wireless drivers turned on.


Partitions need to be swapped but the ramdisk is configured for mmcblk2 so turn off the ramdisk with the bootarg noinitrd and make sure uEnv.txt refers to mmcblk1.

bootargs=console=ttyPS0,115200n8 root=/dev/mmcblk0p2 noinitrd rw rootdelay=5 earlyprintk=serial,ttyPS0,115200n8 
