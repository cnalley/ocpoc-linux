#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x73d7ccd5, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x149c7752, __VMLINUX_SYMBOL_STR(param_ops_uint) },
	{ 0xfda32028, __VMLINUX_SYMBOL_STR(vb2_ops_wait_finish) },
	{ 0xbfd2a30e, __VMLINUX_SYMBOL_STR(vb2_ops_wait_prepare) },
	{ 0x43553fc2, __VMLINUX_SYMBOL_STR(v4l2_event_unsubscribe) },
	{ 0xb352d428, __VMLINUX_SYMBOL_STR(v4l2_ctrl_subscribe_event) },
	{ 0xf7947de1, __VMLINUX_SYMBOL_STR(v4l2_m2m_ioctl_streamoff) },
	{ 0x3c594337, __VMLINUX_SYMBOL_STR(v4l2_m2m_ioctl_streamon) },
	{ 0x9241a93b, __VMLINUX_SYMBOL_STR(v4l2_m2m_ioctl_dqbuf) },
	{ 0x191e346b, __VMLINUX_SYMBOL_STR(v4l2_m2m_ioctl_expbuf) },
	{ 0xe56c1b5, __VMLINUX_SYMBOL_STR(v4l2_m2m_ioctl_qbuf) },
	{ 0x69809a13, __VMLINUX_SYMBOL_STR(v4l2_m2m_ioctl_querybuf) },
	{ 0x5b6ebb27, __VMLINUX_SYMBOL_STR(v4l2_m2m_ioctl_reqbufs) },
	{ 0x7fe42300, __VMLINUX_SYMBOL_STR(v4l2_m2m_fop_mmap) },
	{ 0x8466659a, __VMLINUX_SYMBOL_STR(video_ioctl2) },
	{ 0x825cc50, __VMLINUX_SYMBOL_STR(v4l2_m2m_fop_poll) },
	{ 0xc2b34549, __VMLINUX_SYMBOL_STR(__platform_driver_register) },
	{ 0x44b79255, __VMLINUX_SYMBOL_STR(platform_device_register) },
	{ 0xa67b0b60, __VMLINUX_SYMBOL_STR(platform_device_unregister) },
	{ 0xe62c2e15, __VMLINUX_SYMBOL_STR(platform_driver_unregister) },
	{ 0x6e2bb093, __VMLINUX_SYMBOL_STR(vb2_vmalloc_memops) },
	{ 0x6f800b98, __VMLINUX_SYMBOL_STR(vb2_queue_init) },
	{ 0xde29cc5d, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0xe1127816, __VMLINUX_SYMBOL_STR(v4l2_fh_add) },
	{ 0x1d1ad5aa, __VMLINUX_SYMBOL_STR(v4l2_m2m_ctx_init) },
	{ 0xb3516d99, __VMLINUX_SYMBOL_STR(v4l2_ctrl_handler_setup) },
	{ 0x7a72432e, __VMLINUX_SYMBOL_STR(v4l2_ctrl_new_custom) },
	{ 0x3304b1c1, __VMLINUX_SYMBOL_STR(v4l2_ctrl_new_std) },
	{ 0xe7216b34, __VMLINUX_SYMBOL_STR(v4l2_ctrl_handler_init_class) },
	{ 0xaa88ae2b, __VMLINUX_SYMBOL_STR(v4l2_fh_init) },
	{ 0xe5c63781, __VMLINUX_SYMBOL_STR(kmem_cache_alloc) },
	{ 0x50a30f44, __VMLINUX_SYMBOL_STR(mutex_lock_interruptible) },
	{ 0x7a7f3973, __VMLINUX_SYMBOL_STR(video_device_release) },
	{ 0x17eb0eee, __VMLINUX_SYMBOL_STR(v4l2_m2m_init) },
	{ 0x593a99b, __VMLINUX_SYMBOL_STR(init_timer_key) },
	{ 0x6c2624d0, __VMLINUX_SYMBOL_STR(__video_register_device) },
	{ 0x9d669763, __VMLINUX_SYMBOL_STR(memcpy) },
	{ 0x78538951, __VMLINUX_SYMBOL_STR(video_device_alloc) },
	{ 0xbf6c04cc, __VMLINUX_SYMBOL_STR(__mutex_init) },
	{ 0x2fc30474, __VMLINUX_SYMBOL_STR(v4l2_device_register) },
	{ 0xf8b5fb4b, __VMLINUX_SYMBOL_STR(devm_kmalloc) },
	{ 0xe9309acc, __VMLINUX_SYMBOL_STR(v4l2_m2m_buf_queue) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x91e72433, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0xed7943a7, __VMLINUX_SYMBOL_STR(v4l2_m2m_ctx_release) },
	{ 0xb4774191, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0x59f96e7f, __VMLINUX_SYMBOL_STR(v4l2_ctrl_handler_free) },
	{ 0x53004686, __VMLINUX_SYMBOL_STR(v4l2_fh_exit) },
	{ 0x65ffe0ca, __VMLINUX_SYMBOL_STR(v4l2_fh_del) },
	{ 0xd2a1ef58, __VMLINUX_SYMBOL_STR(video_devdata) },
	{ 0xb81960ca, __VMLINUX_SYMBOL_STR(snprintf) },
	{ 0x328a05f1, __VMLINUX_SYMBOL_STR(strncpy) },
	{ 0x57c846e8, __VMLINUX_SYMBOL_STR(v4l2_m2m_get_vq) },
	{ 0x49f05103, __VMLINUX_SYMBOL_STR(v4l2_m2m_job_finish) },
	{ 0x459e133f, __VMLINUX_SYMBOL_STR(v4l2_m2m_get_curr_priv) },
	{ 0x51d559d1, __VMLINUX_SYMBOL_STR(_raw_spin_unlock_irqrestore) },
	{ 0xf6c8da2a, __VMLINUX_SYMBOL_STR(vb2_buffer_done) },
	{ 0x598542b2, __VMLINUX_SYMBOL_STR(_raw_spin_lock_irqsave) },
	{ 0x4877351, __VMLINUX_SYMBOL_STR(v4l2_m2m_buf_remove) },
	{ 0x7d11c268, __VMLINUX_SYMBOL_STR(jiffies) },
	{ 0x8834396c, __VMLINUX_SYMBOL_STR(mod_timer) },
	{ 0x3bd1b1f6, __VMLINUX_SYMBOL_STR(msecs_to_jiffies) },
	{ 0x4fc8d0ee, __VMLINUX_SYMBOL_STR(vb2_plane_vaddr) },
	{ 0x93b8bd40, __VMLINUX_SYMBOL_STR(v4l2_m2m_next_buf) },
	{ 0x6e42b88, __VMLINUX_SYMBOL_STR(v4l2_device_unregister) },
	{ 0x96b3a810, __VMLINUX_SYMBOL_STR(video_unregister_device) },
	{ 0xd5f2172f, __VMLINUX_SYMBOL_STR(del_timer_sync) },
	{ 0xc6fca5ad, __VMLINUX_SYMBOL_STR(v4l2_m2m_release) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=videobuf2-core,videodev,v4l2-mem2mem,videobuf2-vmalloc";


MODULE_INFO(srcversion, "919B00EEFE2D7CB10F51FF3");
