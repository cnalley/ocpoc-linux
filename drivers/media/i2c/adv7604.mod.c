#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x73d7ccd5, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x51eafc8e, __VMLINUX_SYMBOL_STR(param_ops_int) },
	{ 0x8b9f15cf, __VMLINUX_SYMBOL_STR(i2c_del_driver) },
	{ 0x8c4ef0e0, __VMLINUX_SYMBOL_STR(i2c_register_driver) },
	{ 0x71c42fff, __VMLINUX_SYMBOL_STR(v4l2_async_register_subdev) },
	{ 0xb3516d99, __VMLINUX_SYMBOL_STR(v4l2_ctrl_handler_setup) },
	{ 0x2aac7350, __VMLINUX_SYMBOL_STR(media_entity_init) },
	{ 0x593a99b, __VMLINUX_SYMBOL_STR(init_timer_key) },
	{ 0x43a53735, __VMLINUX_SYMBOL_STR(__alloc_workqueue_key) },
	{ 0xe0f6605b, __VMLINUX_SYMBOL_STR(i2c_new_dummy) },
	{ 0x7a72432e, __VMLINUX_SYMBOL_STR(v4l2_ctrl_new_custom) },
	{ 0x694f7bd2, __VMLINUX_SYMBOL_STR(v4l2_ctrl_new_std_menu) },
	{ 0x3304b1c1, __VMLINUX_SYMBOL_STR(v4l2_ctrl_new_std) },
	{ 0xe7216b34, __VMLINUX_SYMBOL_STR(v4l2_ctrl_handler_init_class) },
	{ 0x6b06fdce, __VMLINUX_SYMBOL_STR(delayed_work_timer_fn) },
	{ 0xb81960ca, __VMLINUX_SYMBOL_STR(snprintf) },
	{ 0x6a343e9f, __VMLINUX_SYMBOL_STR(v4l2_i2c_subdev_init) },
	{ 0x5f754e5a, __VMLINUX_SYMBOL_STR(memset) },
	{ 0xa6b7b107, __VMLINUX_SYMBOL_STR(gpiod_direction_output) },
	{ 0x6b0b1345, __VMLINUX_SYMBOL_STR(__devm_gpiod_get_index) },
	{ 0xd955adc, __VMLINUX_SYMBOL_STR(v4l2_of_parse_endpoint) },
	{ 0x90466c6b, __VMLINUX_SYMBOL_STR(of_graph_get_next_endpoint) },
	{ 0x474d351, __VMLINUX_SYMBOL_STR(of_match_node) },
	{ 0xf8b5fb4b, __VMLINUX_SYMBOL_STR(devm_kmalloc) },
	{ 0x91e72433, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0x7a9ba02e, __VMLINUX_SYMBOL_STR(__v4l2_ctrl_s_ctrl) },
	{ 0xb4774191, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0x8e865d3c, __VMLINUX_SYMBOL_STR(arm_delay_ops) },
	{ 0xe851bb05, __VMLINUX_SYMBOL_STR(queue_delayed_work_on) },
	{ 0x6f344bb9, __VMLINUX_SYMBOL_STR(v4l2_calc_aspect_ratio) },
	{ 0x24d7b4eb, __VMLINUX_SYMBOL_STR(cancel_delayed_work_sync) },
	{ 0x4de0b6ca, __VMLINUX_SYMBOL_STR(gpiod_set_value_cansleep) },
	{ 0xf9a482f9, __VMLINUX_SYMBOL_STR(msleep) },
	{ 0x7eaf8e7a, __VMLINUX_SYMBOL_STR(v4l2_detect_gtf) },
	{ 0x53105839, __VMLINUX_SYMBOL_STR(v4l2_detect_cvt) },
	{ 0x211331fa, __VMLINUX_SYMBOL_STR(__divsi3) },
	{ 0x4103badb, __VMLINUX_SYMBOL_STR(v4l2_print_dv_timings) },
	{ 0x8982d59, __VMLINUX_SYMBOL_STR(v4l2_match_dv_timings) },
	{ 0xfa2a45e, __VMLINUX_SYMBOL_STR(__memzero) },
	{ 0x59f96e7f, __VMLINUX_SYMBOL_STR(v4l2_ctrl_handler_free) },
	{ 0xa912de5e, __VMLINUX_SYMBOL_STR(media_entity_cleanup) },
	{ 0x56edfdc9, __VMLINUX_SYMBOL_STR(v4l2_device_unregister_subdev) },
	{ 0x99842110, __VMLINUX_SYMBOL_STR(v4l2_async_unregister_subdev) },
	{ 0x8c03d20c, __VMLINUX_SYMBOL_STR(destroy_workqueue) },
	{ 0x8f045d1c, __VMLINUX_SYMBOL_STR(cancel_delayed_work) },
	{ 0x6beda9db, __VMLINUX_SYMBOL_STR(i2c_unregister_device) },
	{ 0xfb7d9c45, __VMLINUX_SYMBOL_STR(__udivsi3) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x805b99fd, __VMLINUX_SYMBOL_STR(i2c_smbus_xfer) },
	{ 0x9d669763, __VMLINUX_SYMBOL_STR(memcpy) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

MODULE_ALIAS("of:N*T*Cadi,adv7611*");
MODULE_ALIAS("i2c:adv7604");
MODULE_ALIAS("i2c:adv7611");
