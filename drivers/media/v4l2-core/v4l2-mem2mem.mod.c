#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x73d7ccd5, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0xde29cc5d, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x41112766, __VMLINUX_SYMBOL_STR(vb2_mmap) },
	{ 0x57e63ffd, __VMLINUX_SYMBOL_STR(vb2_create_bufs) },
	{ 0x60ee9172, __VMLINUX_SYMBOL_STR(param_ops_bool) },
	{ 0x91e72433, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0x275ef902, __VMLINUX_SYMBOL_STR(__init_waitqueue_head) },
	{ 0x51d559d1, __VMLINUX_SYMBOL_STR(_raw_spin_unlock_irqrestore) },
	{ 0x50a30f44, __VMLINUX_SYMBOL_STR(mutex_lock_interruptible) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x79a940cd, __VMLINUX_SYMBOL_STR(v4l2_event_pending) },
	{ 0xb4774191, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0xdceaa91d, __VMLINUX_SYMBOL_STR(vb2_qbuf) },
	{ 0xc1f55f73, __VMLINUX_SYMBOL_STR(vb2_querybuf) },
	{ 0xe5c63781, __VMLINUX_SYMBOL_STR(kmem_cache_alloc) },
	{ 0xce3d80ea, __VMLINUX_SYMBOL_STR(vb2_streamon) },
	{ 0xd2a1ef58, __VMLINUX_SYMBOL_STR(video_devdata) },
	{ 0x28e44599, __VMLINUX_SYMBOL_STR(vb2_expbuf) },
	{ 0x1000e51, __VMLINUX_SYMBOL_STR(schedule) },
	{ 0x4c96d858, __VMLINUX_SYMBOL_STR(vb2_reqbufs) },
	{ 0x598542b2, __VMLINUX_SYMBOL_STR(_raw_spin_lock_irqsave) },
	{ 0xd85cd67e, __VMLINUX_SYMBOL_STR(__wake_up) },
	{ 0x344b7739, __VMLINUX_SYMBOL_STR(prepare_to_wait_event) },
	{ 0x920bb860, __VMLINUX_SYMBOL_STR(vb2_dqbuf) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x1cfb04fa, __VMLINUX_SYMBOL_STR(finish_wait) },
	{ 0xf0b43634, __VMLINUX_SYMBOL_STR(vb2_queue_release) },
	{ 0x94d2a074, __VMLINUX_SYMBOL_STR(vb2_streamoff) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=videobuf2-core,videodev";

