#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x73d7ccd5, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x402b8281, __VMLINUX_SYMBOL_STR(__request_module) },
	{ 0x45a44c24, __VMLINUX_SYMBOL_STR(v4l2_ctrl_fill) },
	{ 0x58e4b5aa, __VMLINUX_SYMBOL_STR(i2c_new_probed_device) },
	{ 0x211331fa, __VMLINUX_SYMBOL_STR(__divsi3) },
	{ 0xfa2a45e, __VMLINUX_SYMBOL_STR(__memzero) },
	{ 0xddd44f50, __VMLINUX_SYMBOL_STR(spi_new_device) },
	{ 0x73e20c1c, __VMLINUX_SYMBOL_STR(strlcpy) },
	{ 0x6beda9db, __VMLINUX_SYMBOL_STR(i2c_unregister_device) },
	{ 0xc661874e, __VMLINUX_SYMBOL_STR(module_put) },
	{ 0x638bfab, __VMLINUX_SYMBOL_STR(v4l2_device_register_subdev) },
	{ 0x138d5683, __VMLINUX_SYMBOL_STR(v4l2_subdev_init) },
	{ 0xbfeef8ec, __VMLINUX_SYMBOL_STR(device_unregister) },
	{ 0xb81960ca, __VMLINUX_SYMBOL_STR(snprintf) },
	{ 0x5e515be6, __VMLINUX_SYMBOL_STR(ktime_get_ts64) },
	{ 0x6ad47cba, __VMLINUX_SYMBOL_STR(i2c_new_device) },
	{ 0xf104b6f4, __VMLINUX_SYMBOL_STR(try_module_get) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=videodev";

