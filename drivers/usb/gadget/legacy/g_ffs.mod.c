#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x65137d02, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x149c7752, __VMLINUX_SYMBOL_STR(param_ops_uint) },
	{ 0xb225bf0d, __VMLINUX_SYMBOL_STR(param_ops_ushort) },
	{ 0xd42d6ee9, __VMLINUX_SYMBOL_STR(param_ops_byte) },
	{ 0x1f54e93d, __VMLINUX_SYMBOL_STR(param_array_ops) },
	{ 0x62a79a6c, __VMLINUX_SYMBOL_STR(param_ops_charp) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x8d84106d, __VMLINUX_SYMBOL_STR(ffs_name_dev) },
	{ 0xca24ffa9, __VMLINUX_SYMBOL_STR(ffs_single_dev) },
	{ 0x12da5bb2, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0x1ffc0f99, __VMLINUX_SYMBOL_STR(usb_composite_probe) },
	{ 0x6a014074, __VMLINUX_SYMBOL_STR(usb_remove_function) },
	{ 0x8947e710, __VMLINUX_SYMBOL_STR(usb_add_function) },
	{ 0x16dce863, __VMLINUX_SYMBOL_STR(usb_get_function) },
	{ 0xec4917ed, __VMLINUX_SYMBOL_STR(usb_composite_overwrite_options) },
	{ 0xe551017b, __VMLINUX_SYMBOL_STR(usb_add_config) },
	{ 0x62ae96fd, __VMLINUX_SYMBOL_STR(usb_string_ids_tab) },
	{ 0xcf1e6394, __VMLINUX_SYMBOL_STR(rndis_borrow_net) },
	{ 0x86007c4d, __VMLINUX_SYMBOL_STR(gether_register_netdev) },
	{ 0xc5a02172, __VMLINUX_SYMBOL_STR(gether_set_gadget) },
	{ 0x5dfdcab2, __VMLINUX_SYMBOL_STR(gether_set_dev_addr) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x9b93e305, __VMLINUX_SYMBOL_STR(gether_set_host_addr) },
	{ 0xdecf6962, __VMLINUX_SYMBOL_STR(gether_set_qmult) },
	{ 0xb5581a38, __VMLINUX_SYMBOL_STR(usb_get_function_instance) },
	{ 0xe2d5255a, __VMLINUX_SYMBOL_STR(strcmp) },
	{ 0xe472ef8f, __VMLINUX_SYMBOL_STR(usb_put_function_instance) },
	{ 0x77bb9547, __VMLINUX_SYMBOL_STR(usb_put_function) },
	{ 0x50030c0f, __VMLINUX_SYMBOL_STR(usb_composite_unregister) },
	{ 0x83f9d32e, __VMLINUX_SYMBOL_STR(try_module_get) },
	{ 0xefd6cf06, __VMLINUX_SYMBOL_STR(__aeabi_unwind_cpp_pr0) },
	{ 0xdcdffcfe, __VMLINUX_SYMBOL_STR(module_put) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=usb_f_fs,libcomposite,usb_f_rndis,u_ether";

