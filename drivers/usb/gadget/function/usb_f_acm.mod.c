#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x65137d02, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x9d55148e, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x6e664e0, __VMLINUX_SYMBOL_STR(usb_gstrings_attach) },
	{ 0xf1a0d37f, __VMLINUX_SYMBOL_STR(usb_free_all_descriptors) },
	{ 0x5fc56a46, __VMLINUX_SYMBOL_STR(_raw_spin_unlock) },
	{ 0x39be3e4e, __VMLINUX_SYMBOL_STR(gserial_connect) },
	{ 0x91715312, __VMLINUX_SYMBOL_STR(sprintf) },
	{ 0xf6919b9f, __VMLINUX_SYMBOL_STR(usb_function_unregister) },
	{ 0xe472ef8f, __VMLINUX_SYMBOL_STR(usb_put_function_instance) },
	{ 0x9826517c, __VMLINUX_SYMBOL_STR(dev_err) },
	{ 0xc191db39, __VMLINUX_SYMBOL_STR(usb_ep_autoconfig) },
	{ 0x730e8448, __VMLINUX_SYMBOL_STR(gserial_disconnect) },
	{ 0x2dae7baf, __VMLINUX_SYMBOL_STR(gs_alloc_req) },
	{ 0x87d35cda, __VMLINUX_SYMBOL_STR(config_group_init_type_name) },
	{ 0x33bfdca2, __VMLINUX_SYMBOL_STR(gserial_alloc_line) },
	{ 0x24f71c2, __VMLINUX_SYMBOL_STR(usb_function_register) },
	{ 0xecb465c0, __VMLINUX_SYMBOL_STR(kmem_cache_alloc) },
	{ 0x7f769dde, __VMLINUX_SYMBOL_STR(config_ep_by_speed) },
	{ 0x9c0bd51f, __VMLINUX_SYMBOL_STR(_raw_spin_lock) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x9d669763, __VMLINUX_SYMBOL_STR(memcpy) },
	{ 0xb6652875, __VMLINUX_SYMBOL_STR(gserial_free_line) },
	{ 0x927298b8, __VMLINUX_SYMBOL_STR(usb_assign_descriptors) },
	{ 0x4b553dda, __VMLINUX_SYMBOL_STR(usb_interface_id) },
	{ 0xefd6cf06, __VMLINUX_SYMBOL_STR(__aeabi_unwind_cpp_pr0) },
	{ 0xfe6e13df, __VMLINUX_SYMBOL_STR(gs_free_req) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=libcomposite,u_serial";

