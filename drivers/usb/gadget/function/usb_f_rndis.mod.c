#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x65137d02, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x9d55148e, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x6e664e0, __VMLINUX_SYMBOL_STR(usb_gstrings_attach) },
	{ 0xf1a0d37f, __VMLINUX_SYMBOL_STR(usb_free_all_descriptors) },
	{ 0x97255bdf, __VMLINUX_SYMBOL_STR(strlen) },
	{ 0x80d27775, __VMLINUX_SYMBOL_STR(gether_get_qmult) },
	{ 0x5009953a, __VMLINUX_SYMBOL_STR(gether_setup_name_default) },
	{ 0x4cdd6905, __VMLINUX_SYMBOL_STR(netif_carrier_on) },
	{ 0x81c45386, __VMLINUX_SYMBOL_STR(netif_carrier_off) },
	{ 0xc4e0c979, __VMLINUX_SYMBOL_STR(__dev_kfree_skb_any) },
	{ 0x91e72433, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0x6aed7b5b, __VMLINUX_SYMBOL_STR(gether_get_ifname) },
	{ 0x91715312, __VMLINUX_SYMBOL_STR(sprintf) },
	{ 0x1722f316, __VMLINUX_SYMBOL_STR(skb_realloc_headroom) },
	{ 0x8878cfa6, __VMLINUX_SYMBOL_STR(gether_cleanup) },
	{ 0xf6919b9f, __VMLINUX_SYMBOL_STR(usb_function_unregister) },
	{ 0x692e9ede, __VMLINUX_SYMBOL_STR(skb_trim) },
	{ 0x9b93e305, __VMLINUX_SYMBOL_STR(gether_set_host_addr) },
	{ 0xa9764310, __VMLINUX_SYMBOL_STR(gether_get_dev_addr) },
	{ 0xfa2a45e, __VMLINUX_SYMBOL_STR(__memzero) },
	{ 0x440a40e6, __VMLINUX_SYMBOL_STR(gether_connect) },
	{ 0x30e45904, __VMLINUX_SYMBOL_STR(netif_tx_wake_queue) },
	{ 0xe472ef8f, __VMLINUX_SYMBOL_STR(usb_put_function_instance) },
	{ 0x5a5a94a6, __VMLINUX_SYMBOL_STR(kstrtou8) },
	{ 0x9826517c, __VMLINUX_SYMBOL_STR(dev_err) },
	{ 0x34908c14, __VMLINUX_SYMBOL_STR(print_hex_dump_bytes) },
	{ 0xbf6c04cc, __VMLINUX_SYMBOL_STR(__mutex_init) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0xc191db39, __VMLINUX_SYMBOL_STR(usb_ep_autoconfig) },
	{ 0x826be9b0, __VMLINUX_SYMBOL_STR(free_netdev) },
	{ 0xc5a02172, __VMLINUX_SYMBOL_STR(gether_set_gadget) },
	{ 0xc956e94e, __VMLINUX_SYMBOL_STR(skb_push) },
	{ 0xb4774191, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0xa1ebe383, __VMLINUX_SYMBOL_STR(gether_get_host_addr) },
	{ 0x87d35cda, __VMLINUX_SYMBOL_STR(config_group_init_type_name) },
	{ 0xdecf6962, __VMLINUX_SYMBOL_STR(gether_set_qmult) },
	{ 0x650b65c6, __VMLINUX_SYMBOL_STR(gether_get_host_addr_u8) },
	{ 0xb8f2a833, __VMLINUX_SYMBOL_STR(skb_pull) },
	{ 0x24f71c2, __VMLINUX_SYMBOL_STR(usb_function_register) },
	{ 0xf06c80f, __VMLINUX_SYMBOL_STR(skb_queue_tail) },
	{ 0x86007c4d, __VMLINUX_SYMBOL_STR(gether_register_netdev) },
	{ 0xecb465c0, __VMLINUX_SYMBOL_STR(kmem_cache_alloc) },
	{ 0x5dfdcab2, __VMLINUX_SYMBOL_STR(gether_set_dev_addr) },
	{ 0x7f769dde, __VMLINUX_SYMBOL_STR(config_ep_by_speed) },
	{ 0x36b11f53, __VMLINUX_SYMBOL_STR(gether_disconnect) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x9d669763, __VMLINUX_SYMBOL_STR(memcpy) },
	{ 0x927298b8, __VMLINUX_SYMBOL_STR(usb_assign_descriptors) },
	{ 0x4b553dda, __VMLINUX_SYMBOL_STR(usb_interface_id) },
	{ 0xefd6cf06, __VMLINUX_SYMBOL_STR(__aeabi_unwind_cpp_pr0) },
	{ 0x676bbc0f, __VMLINUX_SYMBOL_STR(_set_bit) },
	{ 0xe983573d, __VMLINUX_SYMBOL_STR(consume_skb) },
	{ 0xbd2dcee1, __VMLINUX_SYMBOL_STR(usb_os_desc_prepare_interf_dir) },
	{ 0x5fae3b17, __VMLINUX_SYMBOL_STR(dev_get_stats) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=libcomposite,u_ether";

